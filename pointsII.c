#include<stdio.h>
#include<math.h>

int polarToCortesian(double *rad_v, double *ang, double *x,double *y) {

	*x = cos(*ang)*(*rad_v);
	*y = sin(*ang)*(*rad_v);
	if (*x == 0.0 && *y == 0) return 0;
	if (*y == 0) return -2;
	if (*x == 0.0) return -1;
	if (*x > 0.0 && *y>0.0) return 1;
	if (*x < 0.0 && *y>0.0) return 2;
	if (*x < 0.0 && *y<0.0) return 3;
	if (*x > 0.0 && *y < 0.0) return 4;
}

int main() {
	double rad_vector, angle,x,y;
	int pos;
	for (int i = 0; i < 3; i++) {
		printf("enter polar coords with space ");
		scanf("%lf %lf", &rad_vector, &angle);
		pos = polarToCortesian(&rad_vector, &angle, &x, &y);
		printf("point position %d x coord = %lf y coord = %lf\n", pos, x, y);
	}

}