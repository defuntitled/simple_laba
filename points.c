#include <stdio.h>
#include <math.h>
#define ETERATIONS 3

int cartesianToPolar(double *x,double*y,double*r,double*a) {
	double rad_vec=sqrt(pow(*x, 2)+pow(*y,2)), ang=atan2(*y,*x);
	*r = rad_vec;
	*a = ang;
	if (rad_vec == 0.0) return 0;
	else return 1;
}

int main() {
	double x,y,r_v,a; // r_v - radius-vector, a - angle
	for (int i = 0; i < ETERATIONS; i++) {
		printf("enter cortesian coords ");
		scanf("%lf", &x);
		scanf("%lf", &y);
		if (cartesianToPolar(&x,&y,&r_v,&a)==0) printf("\npoint is begin of coords system ");
		printf("this point in polar coords: r=%lf,a=%lf radians\n", r_v, a);
	}
}